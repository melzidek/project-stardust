var storageLastUpdated = 'SD_lastUpdated';
var storageSkillData = 'SD_skillData';
var storageDataFetched = 'SD_dataFetched';

// Get a Sheet directly as CSV: https://docs.google.com/spreadsheets/d/13WRvu824lxZ9HWiEnqAGheJpjtOV8KN6Fd3mY9bSSms/export?gid=0&format=csv
// URI of the published(!) Sheet
var spreadsheetUri = 'https://spreadsheets.google.com/feeds/cells/13WRvu824lxZ9HWiEnqAGheJpjtOV8KN6Fd3mY9bSSms/od6/public/values?alt=json-in-script';

async function prepareData() {
    if (!isDataUpdateRequired()) {
        console.log('using cached data');
        return;
    }

    console.log('fetching data');

    var data = await fetchData();
    if (!data) { return; }

    var parsedData = parseData(data);
    if (!parsedData) { return; }

    localStorage.setItem(storageDataFetched, new Date().toUTCString());
    localStorage.setItem(storageLastUpdated, parsedData.lastUpdated);
    localStorage.setItem(storageSkillData, JSON.stringify(parsedData.skillData));
}

function isDataUpdateRequired() {
    var dataFetched = localStorage.getItem(storageDataFetched);

    if (!dataFetched
        || !localStorage.getItem(storageSkillData)
    ) {
        return true;
    }

    var oneDay = 24 * 60 * 60 * 1000;

    return (new Date() - new Date(dataFetched)) > oneDay;

}

async function fetchData() {
    try {
        return await $.ajax(
            {
                url: spreadsheetUri,
                dataType: 'jsonp',
                beforeSend: function (xhr) {
                    xhr.overrideMimeType("application/json; charset=UTF8");
                }
            });
    }
    catch (error) {
        console.log('An error occured while fetching the data');
        return null;
    }
}

function parseData(data) {
    if (!data || !(data.feed) || !(data.feed.entry)) { return null; }

    var feed = data.feed;
    var databank = {
        'lastUpdated': feed.updated['$t'],
        'skillData': {
            characterCodes: []
        }
    }

    var sortedEntries = data.feed.entry.sort((entryA, entryB) => {
        var cellA = getCellObject(entryA);
        var cellB = getCellObject(entryB);

        if (cellA.row !== cellB.row) {
            return cellA.row - cellB.row;
        }

        return cellA.col - cellB.col;
    });

    var columnHeaders = {};
    var rowHeaders = {}
    for (var entry of sortedEntries) {
        var cell = getCellObject(entry);

        if (cell.row === 1
            && cell.col === 1) {
            continue;
        }

        if (cell.row === 1) {
            columnHeaders[cell.col] = cell.content;
            databank.skillData[cell.content] = {};
            continue;
        }
        else if (cell.col === 1) {
            rowHeaders[cell.row] = cell.content;
            databank.skillData.characterCodes.push(cell.content);
            continue;
        }

        databank.skillData[columnHeaders[cell.col]][rowHeaders[cell.row]] = cell.content;
    }

    return databank;
}

function getCellObject(entry) {
    if (!entry || !entry["gs$cell"]) { return null; }

    var cell = entry["gs$cell"];

    return {
        row: Number(cell.row),
        col: Number(cell.col),
        content: cell['$t']
    };
}

function readyFn() {
    $('.checkboxes').append('<h3>Loading data...</h3>');

    prepareData()
        .then(() => {
            prepareUi();
        });
}

function prepareUi() {
    $('.checkboxes').empty();

    if (!localStorage[storageSkillData]) {
        $('.checkboxes').append('<h2>No data could be retrieved</h2>');
        return;
    }

    var data = JSON.parse(localStorage[storageSkillData]);

    $.each(data, function (key, value) {
        if (key !== "characterCodes"
            && key !== "Full Name"
            && key !== "LOCKED"
            && key !== "Generic Buff"
            && key !== ""
            && key !== "Bonus attack"
            && key !== "Call to Assist"
        ) {
            $('.checkboxes').append('<label class="checkbox-row"><input type="checkbox" class="checkbox-input" name="effect" value="'
                + key + '"><img class="effect-img" src="img/effects/'
                + key.replace(/ /g, '') + '.png" alt="' + key + '" /><span class="effect-title">' + key + '</span></label>'
            );
        }
    });

    $('.checkbox-input').change(function () {
        console.log($(this).val() + ' was clicked!');
        //$('.checkbox-row').removeClass('checked');
        $(this).closest('label').toggleClass('checked');


        $('.character-area').empty();
        var sum_keys = [].concat(data.characterCodes);
        var databank_slice = [];

        $('.checkbox-input:checked').each(function (index, item) {
            var matching = _.omit(data[$(item).val()], function (value, key) {
                return _.isEmpty(value);
            });

            databank_slice.push(matching);

            sum_keys = _.intersection(sum_keys, _.keys(matching));
            console.log("Remaining matching characters are: " + sum_keys);
        });
        getMatchingToons(sum_keys, databank_slice);
    });
}

function getMatchingToons(matching_keys, databank_slice) {
    $characterArea = $('.character-area');
    var skillData = getSkillData();

    for (var i = 0; i < databank_slice.length; i++) {
        $.each(databank_slice[i], function (key, value) {
            if (!_.contains(matching_keys, key)) {
                return true;
            }
            var categoryClass = value.replace(',', '-').replace(' ', '').toLowerCase();
            console.log("categoryClass is: " + categoryClass);
            if (!$characterArea.find('.' + categoryClass).length > 0) {
                $characterArea.append('<div class="category ' + categoryClass + '"><div class="category-header">' + titleCase(value) + '</div></div>');
            }

            var charName = skillData && skillData['Full Name'] && skillData['Full Name'][key] ? skillData['Full Name'][key] : '';
            $characterArea.find('.' + categoryClass).append('<img class="portrait ' + key + '" src="img/characters/' + key + '.jpg" alt="' + charName + '" />');
        });
    }
}

function capitalizeFirstLetter(string) {
    if (!string || !string.length) { return ''; }
    if (string.length === 1) { return string.toUpperCase(); }

    return string[0].toUpperCase() + string.slice(1).toLowerCase();
}

function titleCase(string) {
    if (!string || !string.length) { return ''; }

    return string.split(" ").map(x => capitalizeFirstLetter(x)).join(" ");
}

function getSkillData() {
    var skillData = localStorage.getItem(storageSkillData);
    if (!skillData) { return null; }

    return JSON.parse(skillData);
}

$(document).ready(readyFn);